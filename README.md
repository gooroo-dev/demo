# Installing Gooroo.dev in your GitLab Repository

1. In selected repository, create a **Webhook** and point it to `https://gooroo.dev/webhooks/gitlab`.
2. Create an **Access Token** for Gooroo.dev, so it can fetch the code and post comments.
3. Register the repository at your Gooroo.dev control panel
    * Provide repository URL
    * Paste Repository Access Token
    * Paste Webhook Secret Token
4. Now, when a Merge Request created or updated, Gooroo.dev will automatically review it.
