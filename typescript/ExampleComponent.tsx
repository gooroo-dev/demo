import React, { useState, useEffect } from 'react';

interface Props {
  title: string;
  count: number;
}

const ExampleComponent: React.FC<Props> = ({ title, count }) => {
  const [data, setData] = useState<string[]>([]);

  // Mistake 1: Incorrect use of useEffect dependency array
  useEffect(() => {
    fetch('/api/data')
      .then(response => response.json())
      .then(data => setData(data));
  }, [title, count]); // should be [] or [count] if count affects the data

  // Mistake 2: Incorrect type definition for event
  const handleClick = (event: any) => { // 'any' should be a specific type like React.MouseEvent<HTMLButtonElement>
    alert('Button clicked!');
  };

  // Mistake 3: Inefficient rendering by using index as key
  return (
    <div>
      <h1>{title}</h1>
      <ul>
        {data.map((item, index) => (
          <li key={index}>{item}</li> // using index as key can cause inefficient rendering
        ))}
      </ul>
      <button onClick={handleClick}>Click Me</button>
    </div>
  );
};

export default ExampleComponent;
